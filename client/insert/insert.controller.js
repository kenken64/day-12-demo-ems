(function() {
    angular
        .module("EmployeeApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http"];

    function InsertCtrl($http) {
        var vm = this;
        vm.employee = {};
        vm.empNo="";
        vm.employee.firstname = "";
        vm.employee.lastname = "";
        vm.employee.gender = "";
        vm.employee.birthday = "";
        vm.employee.hiredate = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.register = function () {
           
            $http.post("/api/employee", vm.employee)
                .then(function () {
                    vm.status.message =  "The employee is added to the database.";
                    vm.status.code = 202;
                    console.info("success");
                }).catch(function () {
                vm.status.message =  "Failed to add the employee to the database.";
                console.info("error");

            });
        };
    };
})();







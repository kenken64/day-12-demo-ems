(function () {
    angular
        .module("EmployeeApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;


        vm.empName = '';
        vm.result = null;
        vm.showSalary = false;
        vm.names = [
            {
                displayName: "First Name",
                dbName: 'first_name'
            },
            {
                displayName: "Last Name",
                dbName: 'last_name'
            }
        ];

        vm.selectedName = vm.names[0];

        vm.search = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = false;

            $http.get("/api/employee", {
                    params: {
                        'emp_name': vm.empName,
                        'db_name': vm.selectedName.dbName
                    }
                })
                .then(function (result) {
                    console.info("success");
                    vm.result = result.data;

                }).catch(function () {
                console.info("error");
                vm.status.message = "Failed to get the employee from the database.";
                vm.status.code = 400;

            });
        };

        vm.searchForSalary = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = true;
            
            $http.get("/api/employeeSalary/", {
                    params: {
                        'emp_name': vm.empName,
                        'db_name': vm.selectedName.dbName
                    }
                })
                .then(function (result) {
                    console.info("success");
                    vm.result = result.data;

                }).catch(function () {
                console.info("error");
                vm.status.message = "Failed to get the employee salary from the database.";
                vm.status.code = 400;
            });
        };
    };
})();







//Create a modal for employees table
module.exports = function (sequelize, Sequelize) {
    var Salaries = sequelize.define('salaries', {
        emp_no: {
            type:Sequelize.INTEGER,
            primaryKey: true
        },
        salary: Sequelize.INTEGER,
        from_date: Sequelize.DATEONLY,
        to_date: Sequelize.DATEONLY

    }, {
        timestamps: false
    });
    return Salaries;
}




//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");


var Sequelize = require("sequelize");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//Create a connection with mysql DB
var sequelize = new Sequelize('employees', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});


var Employees = require('./model/employee')(sequelize, Sequelize);
var Salaries = require('./model/salaries')(sequelize, Sequelize);

Employees.hasMany(Salaries, {foreignKey: 'emp_no'})
Salaries.belongsTo(Employees, {foreignKey: 'emp_no'})

app.get("/api/employeeInfo", function (req, res) {

    var employeeNo = req.query.emp_no;

    Employees
        .find({
            where: {
                emp_no: employeeNo
            },
            order: [
                ['emp_no', 'ASC'],
                [Salaries, 'from_date', 'DESC']
            ],
            limit: 10,
            include: [
                Salaries
            ]
        })
        .then(function (result) {
            if (result && result.dataValues) {
                res.json(result.dataValues);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });

});


app.put("/api/employeeInfo/", function (req, res) {
    console.log(req.body);

    Employees.find({where: {emp_no: req.body.emp_no}}).then(function (result) {
        if (result) {
            result.updateAttributes(
                {
                    first_name: req.body.emp_name
                }
            ).then(function (data1) {
                console.log(data1);
            })
        }

    });

});

app.delete("/api/deleteSalary/:empNo", function (req, res) {
    console.log('-----------',req.params.empNo);
    Salaries.findOne(
        { where :{
            'emp_no' :req.params.empNo
            },
            order: [
                ['from_date', 'DESC']
            ]
        }
    ).then(function (response) {
       // console.log('----response-----------',response.dataValues);
        Salaries.destroy({
            where: {
                emp_no : req.params.empNo,
                from_date : response.dataValues.from_date.toISOString().split('T0')[0]
            }
        }).then(function (result) {
            if(result=="1")
                res.json({success:true});
            else
                res.json({success:false});

        });
        })


});


app.post('/api/employee', function (req, res) {
    console.log("hihi");


    //var emp_no = genereateEmpNo(); //alter emp_no to be AUTOINCREMENT for now.

    var gender = req.body.gender == "male" ? 'M' : 'F'; // enum value in the table
    var birth = req.body.birthday.substring(0, req.body.birthday.indexOf('T')); // format date
    var hire = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T')); // format date
    var lastname = req.body.lastname;
    var firstname = req.body.firstname;
    var emp_no = req.body.empNo;


    Employees.create({
        emp_no: emp_no,
        birth_date: birth,
        first_name: firstname,
        last_name: lastname,
        gender: gender,
        hire_date: hire

    }).then(function (result) {
        console.log('--------------', result);
        if (result && result.dataValues) {
            res.json(result.dataValues);
        } else {
            res.status(400).send(JSON.stringify("Record Not Found"));
        }

    });

});

app.get('/api/employee', function (req, res) {

    var employeeName = req.query.emp_name;
    var employeeDBName = req.query.db_name;
    var whereObj = {}
    whereObj[employeeDBName] = employeeName;
    console.log('-----------', whereObj);
    Employees
        .findAll({
            where: whereObj,
            limit: 10
        })
        .then(function (result) {

            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });

});

app.get('/api/employeeSalary', function (req, res) {
    var employeeName = req.query.emp_name;
    var employeeDBName = req.query.db_name;
    var whereObj = {}
    whereObj[employeeDBName] = employeeName;
    // This will select only salary from the Salaries table, and only all employee info from the Employee table
    Employees
        .findAll({
            where: whereObj,
            order: [
                ['emp_no', 'ASC'],
                [Salaries, 'from_date', 'DESC']
            ],
            limit: 10,
            include: [
                Salaries
            ]
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });

});


//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});
